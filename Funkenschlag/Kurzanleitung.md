# Funkenschlag - Kurzanleitung
## Spielerzahlvariationen
![Spielerzahlvariationen](Spielerzahlvariationen.jpg)

## Wichtige Infos
- Rohstoffe können zwischen Kraftwerken jederzeit hin- und hergeschoben werden
- **Das niedrigste Kraftwerk im Markt hat immer eine größere Nummer als die höchste Anzahl angeschlossener Städte eines Spielers!**

## Spielvorbereitung
- Spielgebiete auswählen (Zahl der Gebiete siehe Spielerzahlvariationen)
- Startgeld: 50 Dollar
- Spielerreihenfolge (**Nicht nur Startspieler!**) auslosen (**Wird nach der ersten Kraftwerksversteigerung neu festgelegt!**)
- Rohstoffmarkt auffüllen (Uran bis 14, Müll bis 7, Öl bis 3, Kohle bis 1)
- Kraftwerksmarkt bilden (03-10)
- Kraftwerk Nr. 13 auf den Nachziehstapel, Stufe 3 Karte unter den Nachziehstapel

## Spielablauf
### Phase 1: Reihenfolge festlegen
Anzahl der Häuser auf dem Plan, bei Gleichstand höhere Kraftwerksnummer -> bester Spieler

### Phase 2: Kraftwerke kaufen
- Startgebot = Kraftwerksnummer
- In Spielerreihenfolge aus der oberen Reihe (**in Stufe 3 aus allen 6**) aussuchen oder passen (**In der ersten Runde muss ein Kraftwerk ausgesucht werden!**)
- Wer einmal beim aussuchen gepasst hat, darf diese Runde nicht mehr aussuchen oder mitbieten
- Nun wir im Uhrzeigersinn erhöht oder gepasst
- Man darf als Spieler an der Reihe so lange ein neues Kraftwerk aussuchen, bis man eines ersteigert hat

- Verkaufte Kraftwerke werden sofort vom Nachziehstapel ersetzt
- **Die Reihenfolge der Kraftwerke im Markt ist immer sortiert!**

### Phase 3: Rohstoffe kaufen
- In umgekehrter Spielerreihenfolge, soviele Rohstoffe wie man möchte (Jedes Kraftwerk kann doppelt so viel lagern, wie es in einem Zug verfeuern kann)

### Phase 4: Bauen
- In umgekehrter Spielerreihenfolge, soviele Häuser wie man möchte
- Anschlusskosten + Verbindungskosten
- Städte dürfen beliebig übersprungen werden, auch wenn von anderen besetzt
- Max. Häuser pro Stadt = Spielstufe (2. Anschluss erst ab Stufe 2 etc.)
- **Man ist zu keiner Zeit gezwungen zu bauen, auch nicht in der ersten Runde!**


### Phase 5: Bürokratie
1. Geld kassieren
  - Entscheiden wie viele Rohstoffe man verfeuert (Es müssen nicht alle Kraftwerke genutzt oder alle Orte die man versorgen kann versorgt werden!)
  - Geld für versorgte Orte kassieren (können weniger sein als angeschlossen aber nie mehr!)
2. Rohstoffe auffüllen (siehe Tabelle am Ende)
3. Kraftwerksmarkt aktualisieren
  - Stufe 1 + 2: Höchstes Kraftwerk unter den Stapel
  - Stufe 3: Niedrigses Kraftwerk entfernen und nachziehen

## Stufen
### Stufe 2
- Beginnnt vor Phase 5
- **DANN:** Kraftwerk der niedrigsten Nummer entfernen und nachziehen

### Stufe 3
- Wenn die Karte gezogen wird, wird der restliche Nachziehstapel gemischt

- In Phase 2 kommt die Karte als höchstes Kraftwerk in den Markt, nach Phase 2 wird sie mit dem niedrigsten Kraftwerk entfernt
- Ab sofort immer 6 Karten im Markt (**Es darf auf alle geboten werden!**)

## Spielende
- Das Spiel endet in **Phase 5**, wenn in **Phase 4** ein oder mehrere Spieler min. die entsprechende Anzahl an Städten angeschlossen haben
- Gewonnen hat der Spieler mit dem höchsten Einkommen in dieser Runde (muss nicht der Spieler mit den meisten Häusern sein!), bei Gleichstand der mit mehr Bargeld

![Rohstoffe nachfüllen](Rohstoffe.jpg)
