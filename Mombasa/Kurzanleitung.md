# Kurzanleitung
## Einleitung:
- Es gewinnt der Spieler mit dem größten Vermögen, dieses setzt sich zusammen aus:
  - Bargeld
  - Wert aller Kompanieanteile
  - letzter überquerter Wert auf der Diamantenleiste
  - letzter überquerter Wert auf der Buchhaltungsleiste
  
## Vereinfachtes Spiel ohne Bücher
Das Spiel würde auch Prima ohne Bücher funktionieren (der Meinung waren wir alle), man muss sich nur was für die Buchhalterkarten überlegen.

## Vorbereitung
- Kompanieleisten an Spielplanseiten anlegen 
  - **Im ersten Spiel:**(A1 Schwarz, B1 Rot, C1 Weiß, D1 Orange)
  - In folgenden Spielen: Per Leistenkarten ermitteln welche Leiste zu welcher Kompanie gehört
- Alle Handelsposten auf Kompaniebasis ihrer Farbe
- Aktionskarten nach Buchstaben in oberer rechter Ecke sortieren, innerhalb Buchstabe mischen und aufeinander (E unten, A oben)
- Karteauslage vom Aktionskartenstapel füllen, rechte, mittlere, dann linke Spalte
- Rundenleiste mit Münzen füllen
  - **2 Spieler**: Feld 2-7 je 1
  - **3 Spieler**: Feld 2,4,6 je 2 Münzen; Feld 3,5,7 je 1 Münze
  - **4 Spieler**: Feld 2-7 je 2
- Buchplättchen nach Rückseite sortieren und in 3 Stapeln neben Spielplan, von jedem Stapel 4 Bücher aufgedeckt in Bücherauslage zum passenden Buchstaben
- 4 Bonusplättchen offen neben Spielplan

- Jeder Spieler wählt eine Farbe und erhält:
  - Spielerablage, 9 Startaktionskarten, 4 Anteilsmarker (flach), 1 Diamantmarker, 1 Tintenfass, 1 Pfund
  - Bonusmarker: 2 Spieler: jeder 3, 3-4 Spieler: jeder 2

- Startspieler erhält "1er" Ausbreitungskarte, im Uhrzeigersinn 2er, 3er, 4er und fügt sie den Handkarten hinzu

- Startplättchen:
  - **Im ersten Spiel:** 1. Spieler: Startplättchen mit Mombasa, 2. Cape Town, 3. Saint-Louis, 4. Cairo
  - In folgenden Spielen: 10 Startplättchen mischen und jedem eins verdeckt geben
  
- Jeder Spieler: 
  - Startplättchen offen auf Umschlagfeld
  - Tintenfass auf Tintenfass auf dem Startplättchen
  - Diamant auf Diamant
  - Aus den Start-Handkarten die 3 abgebildeten oberhalb der Spielerablage (Sammelslots) in beliebiger Reihenfolge auslegen
  - Startbonus erhalten

## Besondere Regeln/Hinweise/Limits
- Jede Spielerablage verfügt oben über Sammelslots, unten über Aktionsslots. Die 3 in Spielerfarbe markierten stehen von Spielbeginn an zur Verfügung
- Der Tintenfass-Marker darf auf der Buchhaltungsleiste nur Felder überqueren oder darauf stehenbleiben, wo Buchplättchen liegen
- Erhaltene Buchhaltungspunkte müssen sofort eingelöst werden und können nicht aufgehoben werden
- Bücher auf der Buchhaltungsleiste werden niemals wieder entfernt oder umgelegt, können aber mit neuen Büchern überdeckt werden. Ein A-Buch darf aber niemals über ein B- oder C-Buch gelegt werden
- Fehlende Bücher in der Bücherauslage werden sofort nach dem Zug aufgefüllt!
- Die Kartenauslage wird erst am Ende der Runde wieder aufgefüllt!
- Der Aktionskarten-Ablagestapel wird nicht gemischt, wenn der Aktionskartenstapel leer ist werden keine Aktionskarten nachgelegt.
- Pro Kompanieleiste darf jeder Spieler nur einen Bonusmarker pro Runde auf die Extra-Bonusfelder setzen!
- Der zweite Dauerbonus einer Kompanieleiste ersetz den ersten Dauerbonus (ist eine verbesserte Version! siehe Seite 11 - etwas unklar in Anleitung)

### Kompanieleisten und ihre Boni
- Auf Kompanieleisten finden sich ein oder zwei Pflichteinzahlungen. Immer wenn eine Pflichteinzahlung passiert wird, muss sofort der Geldbetrag bezahlt werden. Kann oder will man dies nicht, muss man vor der Pflichteinzahlung stehen bleiben.
- Jede Kompanieleiste hat zwei Sonderfelder. Immer wenn man ein solches erreicht oder überschreitet erhält man sofort den Betrag. **Spieler die auf der Leiste schon weiter sind erhalten diesen Betrag nun ebenfalls aus der Bank**
  - Jedes Sonderfeld schaltet einen Leistungsbonus frei: Dauerbonus oder Extra-Bonusfeld - Diese Leistungsboni stehen **ab dem nächsten Zug** zur Verfügung
Dauerboni können immer bei Aktionen genutzt werden, für die sie relevant sind (nicht ohne Karte nutzbar), Extra-Bonusfelder können wie normale Bonusfelder mit einem Bonusmarker besetzt werden. **In jeder Runde darf man nur einen Bonusmarker pro Kompanieleiste setzen.**

### Besondere Felder der Spielerablage
- Es gibt ein Feld auf der Diamantenleiste, das einen zusätzlichen Aktionsslot freischaltet, wenn der Diamantenmarker das Feld erreicht. Dieser Aktions-Slot darf in allen folgenden Planungsphasen genutzt werden
- Es gibt ein Feld auf der Buchhaltungsleiste, wenn dieses vom Tintenfass-Marker erreicht wird, wird hier der Aktions-Slot freigeschaltet
- Wird bei einer der beiden Leisten das Schlussfeld erreicht, gibt es statt weiterem Vorrücken die abgebildete Belohnung.


## Spielablauf
### Planungsphase
Alle Spieler gleichzeitig:
1. Karten verdeckt in den Aktionsbereich legen
2. Alle Spieler decken die Karten im Aktionsbereich auf

- Es dürfen **max.** so viele Karten ausgelegt werden, wie Aktionsslots zur Verfügung stehen (am Anfang 3).
- Die Position ist frei wählbar aber wichtig wegen der Sammelslots (1:1 Verknüpfung oberhalb mit unterhalb der Spielerablage)

### Allgemeine Aktionsphase
Reihum immer genau eine Aktion aus den Folgenden, bis alle Spieler ausgestiegen sind :

1. **Ein oder mehrere** Warenkarten einer Sorte (Kaffee, Bananen oder Baumwolle) nutzen
2. **Alle** Ausbreitungskarten nutzen
3. **Eine** Buchhalterkarte nutzen
4. **Eine** Diamanthändler-Karte nutzen
5. **Einen** Bonusmarker setzen
6. Aktionsphase beenden

Es können nur Aktionskarten aus dem Aktionsbereich gewählt werden!

#### Warenkarten nutzen

Warengesamtmenge = Summe der Einheiten der Karten gleicher Ware

Mit Warenkarten kann man entweder
- Maximal **eine** Karte aus der Auslage erwerben und auf die Hand nehmen
  - Der Kartenpreis beträgt den roten Kistenwert der Karte + den vom Spielplan - Die Warengesamtmenge muss >= Kartenpreis sein 
Übersteigt die Warenmenge den Kistenpreis, darf der Rest für das Vorrücken auf den Kompanieleisten verwendet werden (s.u.)

- Auf ein oder mehreren Kompanieleisten vorwärtsziehen
  - Pro Einheit der (verbliebenen/überschüssigen) Warengesamtmenge darf man auf einer beliebigen Kompanieleiste ein Feld vorwärtsziehen (beliebig aufteilbar)

**Verwendete Warenkarten auf Rückseite drehen**
**Die Kartenauslage wird erst am Ende der Runde wieder aufgefüllt!"

#### Alle Ausbreitungskarten nutzen

- Mit der Gesamtsumme **aller** Ausbreitungskarten im Aktionsbereich **eine** Kompanie ausbreiten.
- Das Ausbreiten kostet pro Region 1, 2 oder 3 Ausbreitungspunkte.
- Es müssen nicht alle Ausbreitungspunkte verbraucht werden

**Zielregion:**
- Die Zielregion muss benachbart zur Basis der Kompanie oder benachbart zu einer Region der Kompanie sein. Dabei zählen auch im selben Zug eingenommene Regionen als benachbart.
- Die Zielregion darf einen Handelsposten einer anderen Kompanie enthalten, nicht aber der aktiven Kompanie.

**Kosten:**
- Einfache Grenze 1, Doppelte Grenze 2
- Bereits besetzt mit dem Handelsposten einer anderen Kompanie: Zusatzkosten 1

**Haus wählen:**
- Spalte frei wählbar
- Innerhalb der Spalte immer das oberste Haus

- **Haus hochkant stellen um zu markieren, dass die Belohnung für die Region noch nicht erhalten!**

- Nachdem die Ausbreitungspunkte aufgebraucht sind, Belohnungen der eingenommenen Regionen erhalten
  - Münzen aus der Bank nehmen / Schritte auf Diamantenleiste / Anteilsmarker der aktiven Kompanie vorrücken / Buchhaltungspunkte in Bücher aus Auslage oder Geld tauschen
    1 Geld: 1 Punkt, A/B-Bücher: 1 Punkt, C-Bücher: 2 Punkte

##### Buchhaltung / Bücher

Wenn min. ein Buch erworben wurde, werden nun alle erworbenen Bücher auf der Buchhaltungsleiste platziert (egal wo, keine A-Buch über B/C-Bücher oder auf Felder mit durchgestr. A)


- Wurden Handelsposten in besetzte Regionen gesetzt, wird dort die Handelskompanie verdrängt
  - Das Häuschen wird zurück bei der Kompanie in eine beliebige Spalte auf das unterste, freie Feld gesetzt (das  Schlussfeld (allerunterstes) darf, sofern es einmal leer war, nicht wieder besetzt werden!)
  - Falls es kein erlaubtes Feld in der Kompaniebasis gibt, kommt das Häuschen aus dem Spiel

- Hochkante Häuser richtig stellen

**Alle Ausbreitungskarten auf Rückseite drehen**


#### Eine Buchhalter-Karte nutzen

1. Für 2 Geld **darf max ein** Buch auf der Buchhaltungsleiste auf die Rückseite gedreht werden (In der Not)
**Umgedrehte Bücher dürfen immer betreten werden, sie haben keine Voraussetzungen mehr**
2. Tintenfass-Marker soweit vorziehen, wie man mag und kann. Es darf nur auf Felder mit einem Buch vorgezogen werden, für jedes Buch müssen die 1 bzw. 2 Voraussetzungen der oberen Hälfte erfüllt sein. 
**Bücher verbrauchen die Aktionskarten nicht!**
3. Wenn du nicht mehr vorziehen kannst oder willst, bleibt der Tintenfass-Marker auf dem Buch und **nur von diesem Buch** erhältst du die Belohnung.
4. Nach dem Vorwärtsziehen tausche die Anzahl der Buchhaltungspunkte ein, die auf der gewählten Karte abgebildet sind.

**Verwendete Buchhalter-Karte auf Rückseite drehen**

#### Eine Diamantenhändler-Karte nutzen

Entsprechend der Diamantenhändler-Karte Diamanten-Marker vorrücken und Geld nehmen, dann ggf. Minensymbole in kontrollierten Regionen (1 Schritt / 2 Minensymbole) der angegebenen Kompanie zählen und Diamanten-Marker vorziehen

**Verwendete Diamantenhändler-Karte auf Rückseite drehen**

#### Einen Bonusmarker setzen

1. Einen eigenen Bonusmarker auf leeres Bonusfeld setzen, wenn Voraussetzungen erfüllt bzw. Kosten bezahlt. 
2. Bonus entweder sofort erhalten bzw. Bonusplättchen für nächste Runde sichern.

Das Bonusfeld ist für den Rest der Runde blockiert.

- Das Startspieler-Bonusfeld darf **vom Startspieler nicht mit seinem ersten Zug** der Runde besetzt werden, danach schon.
- **Eine** Karte aus der Auslage kaufen und mit Geld statt Waren bezahlen
- Karte **aus der Hand** abwerfen (offen auf den Aktionskarten-Ablagestapel und Kistenwert + 2 Geld erhalten
- **Mehrheiten-Bonusfelder:** Kein anderer Spieler darf **mehr** Einheiten der Warensorte **offen** liegen haben. Es dürfen immer  auch immer "schlechtere" Belohnungen als die höchstmögliche gewählt werden.
- **Bonusplättchen-Felder:** Das Bonusplättchen erhält man in der Vorbereitungsphase der nächsten Runde!
  - +1 Warenplättchen: In einem beliebigen Zug auf untere Hälfte einer Warenkarte legen. Der Warenwert dieser Karte ist für alle Belange um 1 erhöht. Wurde die Warenkarte verwendet, drehe sie mit Bonusplättchen auf die Rückseite.
  - Übrige Bonusplättchen: Die übrigen Bonusplättchen werden wie eine Aktionskarte der jeweiligen Sorte genutzt, belegen aber keinen Aktions-Slot.
    *Diamantenhändler-Bonusplättchen:* Nach dem Vorrücken des Diamanten-Markers um 2, wird der Marker noch um 1 weiteres Feld für jeden weiteren Diamanthändler-Karte, der offen im Aktionsbereich liegt vorgezogen

#### Aktionsphase beenden

1. Genau **eine** der Sammelreihen aufnehmen
2. Aktionsbereich aufräumen
- Alle Karten wieder auf die Vorderseite drehen und vom Aktionsslot zum verknüpften Sammelslot verschieben. 
  - Falls sich dort schon Karten befinden, füge die Karten am Ende der Reihe ein. Das obere linke Symbol jeder Karte muss sichtbar sein.
- Bonusplättchen zurücklegen

### Vorbereitungsphase

#### Bücherauslage-Münzen

- Münzen von der Rundenliste auf entsprechende Felder der Bücherauslage legen.
- Falls es zwei Münzen sind, kommt die zweite auf die untere Reihe der Bücherauslage. 
- Spieler die ein Buch mit Münzen kaufen, erhalten die Münze dazu

#### Kartenauslage auffüllen

- alle übrig gebliebenen Karten der rechten Spalte auf den Aktionskarten-Ablagestapel
- Karten der Zeile bündig nach ganz rechts schieben
- beginnend mit der rechten Spalte, von oben nach unten, jedes leere Feld mit einer Karte vom Aktionskartenstapel füllen

Die letzten 8 Karten des Aktionskartenstapels sind zusätzliche Anteilskarten. Sie bleiben bis zur Schlusswertung in der Hand und dürfen nicht in einen Aktions-Slot gelegt werden.

#### Bonusmarker (mit evtl. Bonusplättchen) zurücknehmen

Bonusmarker in eigenen Vorrat nehmen. Bonusplättchen offen in Aktionsbereich legen

**Es beginnt die nächste Planungsphase**

## Spielende

Nach der allgemeinen Aktionsphase der 7. Runde endet das Spiel. Nun findet die Schlusswertung statt.

### Schlusswertung

1. Alle Sammelreihen zurück auf die Hand nehmen und alle Karten heraussuchen, die einen zusätzlichen Kompanieanteil in der unteren linken Ecke zeigen. Diese werden raussortiert vor sich abgelegt.
2. Bargeld zählen und aufschreiben
3. Wert für Anteile an jeder Kompanie berechnen: Freiliegende Münzen mit Anteilen (Von der Anteilsleiste + von Karten) multiplizieren und für jede Kompanie aufschreiben
4. Diamantenleiste ablesen und  letzten **überschrittenen** Wert aufschreiben
5. Buchhaltungsleiste: Letzten **überschrittenen** Wert aufschreiben

Alle Werte addieren. **Kein Tie-Break**.