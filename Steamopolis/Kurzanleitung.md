# Kurzanleitung - Steamopolis:

## Einleitung:
- Sieg nach SP (Stimmpunkte) - man will zum Bürgermeister von Steamopolis gewählt werden
- Siegpunkte gibt es durch Banner, Maschineneffekte oder bei Aktionen und Einkäufen

## Vorbereitung:
1. Richtige Seite vom Spielplan 1-3 oder 4 Spieler
2. Alle 8 Statuen rechts am Rand auf Statuenfelder
3. Alle Marktfelder (Stufe 0-3) mit 1 zufälligen Maschinenteil der entspr. Stufe (Stufenzahl ergibt sich halb aus Spielplan, halb aus Plättchen)
   In jeder Reihe und auf jeder Seite zeigt immer nur das Plättchen mit der niedrigsten Stufe die Maschinenseite, alle anderen die Bannerseite
4. Bonusmarker platzieren (Weiße Steine)
5. Passagierfiguren auf Ebene 4 und 6
6. Alle Passagierplättchen gemischt, oberste offen auf Stapel
7. Kristalle, Zahnräder, Spendengala- und Sponsorplättchen bereitlegen
9. Jeder Spieler zieht 1 Charakterplättchen, Startspieler ist der mit der niedrigsten Nummer
12. Basisdruckmarker auf markierten Platz
13. 3 Drucksteine (Seite ohne Pfeil) auf die ersten Felder der Druckanzeige, die anderen beiden bereitlegen
14. Schmarotzer bereitlegen
15. Zufälliges Startmaschinenplättchen ziehen, 1 Kapazitätsplättchen nehmen
16. Effekte des Charakterplättchens von oben nach unten ausführen
17. Wähle die Seite des Startmaschinenplättchens und setze beide Maschinen an beliebiger Stelle in den Maschinenraum

## Besondere Regeln/Hinweise/Limits:
- Drucksteine mit Pfeil
  - Jeder Druckstein mit Pfeil bewegt sich am Anfang jedes Zuges (unabhängig von der Aktion und auch im finalen Zug) genau einmal
  - Die Reihenfolge der Bewegungen der Drucksteine mit Pfeil ist beliebig
- Bonusmarker blockieren die Position für den Basisdruckmarker, können aber jederzeit freiwillig entfernt werden
- Bereits eingelagerte Ressourcen dürfen durch neu gewonnene ersetzt werden
- Seinen letzten im Spiel befindlichen Druckstein darf man nicht ausgeben
- Drucksteine mit Pfeil verlieren ihren Pfeil **ausschließlich** durch die Kosten der oberen Stadtteileffekte
- Der Basisdruckmarker darf höchsten das Feld mit der 4 erreichen (S. 15)
- Beim Einkaufen von Bonusmarkern (Ebene 1) dürfen beliebig viele Bonusmarker mit einer Aktion gekauft werden (S. 17)
- Beim Platzieren eines Bonusmarkers werden Drucksteine nach rechts weggeschoben
- Schmarotzer
  - Hat die Maschine Aktivierungseffekt UND Passiveffekt, muss man sich entscheiden. Der Schmarotzer wird nur aktiviert, wenn der entsprechende Effekt aktiviert wurde (S. 33)
  - Im Spiel mit 2 Spielern dürfen bis zu 2 Schmarotzer pro Maschine platziert werden
  - Im Spiel mit 3 Spielern dürfen mehrere Schmarotzer bei einem Spieler platziert werden, aber nicht auf Maschinen wo bereits *eigene* Schmarotzer liegen
  - Der Besitzer eines Schmarotzers darf den Effekt auch nutzen, wenn der Besitzer der **aktivierten Maschine** auf den Effekt verzichtet!

## Spielablauf:
Wähle eine von 3 möglichen Aktionen:
<ol type="a">
  <li>Dampf machen</li>
  <li>1 Ziel festlegen</li>
  <li>Alle Ziele anfliegen</li>
</ol>

### Dampf machen
1. Dampfkessel zählen (jeder Dampfkessel zählt mit, egal ob oben oder unten auf Maschine) = N
2. N mal einen einzelnen Schritt mit einem beliebigen Druckstein. Blockierte Felder werden übersprungen
   Überschüssiger Druck verfällt, man muss den Druck aber so viel wie möglich erhöhen
   
### 1 Ziel festlegen
Diese Aktion ist nur durchführbar, wenn man einen Druckstein auf der Druckanzeige hat und den gewählten Stein auch einsetzen kann
1. Druckstein auswählen
2. (Optional) Druck durch Maschinen leiten
3. Druckstien entsprechend dem (Rest-)druck auf dem Spielplan einsetzen
   Der Stein darf auf jedem beliebigen freien Aktionsfeld eingesetzt werden, dessen Etage gleich oder niedriger als der Druck ist
   
#### Druck durch die Maschinen leiten
Möchte man den Druck durch die Maschinen leiten, muss der Druckstein an der aktuellen Position nach unten gesetzt werden.
Er darf nur durch die Maschinen geleitet werden, wenn er auch danach noch einsetzbar ist.

Von dort an darf er nach links durch beliebig viele oder wenige Maschinen geleitet werden, allerdings werden alle Maschinen zwischen Start und Ziel aktiviert.

Ressourcen durch Maschinenaktivierungen werden sofort eingelagert, solange Lagerplätze frei sind.
So gewonnene Ressourcen dürfen bei folgenden Maschinen, deren Anwendung Ressourcen kostet direkt verwendet werden.

Mit dem Restdruck wird der Stein auf dem Spielplan eingesetzt.

### Alle Ziele anfliegen
Diese Aktion darf auch gewählt werden, wenn nicht alle Drucksteine auf dem Spielplan sind.

*Alle* Drucksteine auf dem Spielplan werden in beliebiger Reihenfolge ausgewertet

Pro Druckstein folgende Schritte:
1. Wähle 1 Druckstein
2. Druckstein zurücknehmen (rechts auf das erste freie Feld nach dem Basisdruckmarker setzen, ist kein Platz frei nimm einen Bonusmarker aus dem Spiel, geht dies nicht, kommt der Druckstein zurück in den Vorrat)
3. Passagier auf Ebene des Drucksteins aufnehmen oder absetzen (Aufnehmen nur möglich, wenn er in diesem Zug auch noch abgesetzt wird!)
4. Stadtteileffekt ausführen **ODER** im Markt der Etage einkaufen **ODER** Aktion verfallen lassen (S. 17)
5. Ressourcen einlagern
#### Passagierziele
Ein Passagier möchte immer zum groß gedruckten Ziel, es sei den er befindet sich bereits in dieser Ebene, dann möchte er zum kleingedruckten Ziel.

Die Belohung für die Beförderung eines Passagieres erhalt man am Ende seines Zuges.

#### Einkaufen
- Statt einen Stadtteileffekt auszuführen, darf **1** Plättchen der niedrigsten vorhandenen Stufe der Etage des Drucksteins gekauft werden.
- Man entscheidet sich, ob man das Plättchen als Maschine (an beliebiger Position) oder als Banner (gibt sofort Siegpunkte) einsetzen möchte.
- Es gibt Kosten, die abhängig von der Einsatzart des Plättchens anfallen (Maschine 1. Reihe oder Banner 2. Reihe) **UND** Kosten die unabhängig von der Einsatzart anfallen (3. Reihe)
Alle aufgedeckten (bevor das Plättchen weggenommen wird) Kosten müssen gezahlt werden, sowohl die auf dem Spielplan (1. - 3. Reihe) als auch die auf dem Maschinenteil (3. Reihe).

#### Banner
- **Lila Banner:** Geben Siegpunkte pro Banner, zählen sich selber mit, bestehen aus  **ZWEI** Bannern!
- **Orangene Banner:** Geben Siegpunkte entsprechend der Anzahl eigener eingebauter Maschinen
- **Blaue Banner:** Geben Siegpunkte entsprechend der Anzahl eigener Passagierplättchen

## Spielende
Das Spielende wird eingeleitet, sobald die 3. Statue errichttet wird (indem 3 Märkte leergekauft wurden) oder sobald der Passierstapel aufgebraucht ist. Die aktuelle Runde (alle Spieler bis vor den Startspieler) wird zu Ende gespielt und es folgt die **finale Runde**

### Finale Runde
In Spielerreihenfolge dürfen alle Spieler je **1 finalen Zug** ausführen.

1. Dampf machen
2. **1** Ziel festlegen, auch wenn dies bereits von Mitspielern (nicht von eigenem Druckstein) besetzt ist
3. Alle Ziele anfliegen

### Gleichstand
1. (Maschinen + Bannerplättchen) vergleichen - 2. Spielerreihenfolge
