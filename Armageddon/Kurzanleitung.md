# Kurzanleitung:

## Vorbereitung:
- Jeder Spieler erhält einen Anführer (Lila - kann als jede Farbe eingesetzt werden) und 5 Arbeiter (grün)
- 2 Wertungsplättchen ziehen und offen auf die Rundenanzeige legen
- 3 Bietplättchen (Dominoteile, rot/grün) ziehen nuter Bietzonen legen und Rest verdeckt beseitelegen
- Karten nach Rückseite sortieren, mischen und bereitlegen
- Gebäudeplättchen mischen und verdeckt neben Gebäudefelder legen
- Rundenmarker *vor* das erste Rundenfeld legen

- Jeder Spieler legt ein Ausbauplättchen auf die Extension
- Jeder einen Spielermarker auf 10 Punkte und auf das erste Feld (vor Nr. 1) der Plündererleiste
- Schadensmarker mit Explosionsseite auf das 3. Feld der Schadensleiste (sodass links danenben das +Plünderer Symbol frei ist)
- Jeder Spieler zieht 2 Wertungsplättchen und legt sie offen hinter Sichtschirm (am Ende muss er davon eins aussuchen)

## Besondere Regeln/Hinweise/Limits:
- In der ersten Runde darf auf jeder Ausrüstungskarte max. 1 Plünderer stehen, weitere Plünderer kommen zurück in den Beutel und es wird neu gezogen bis entsprechend andere Figuren gezogen wurden!
- In Runde 1-3 werden die Level 1 Gebäudekarten ausgelegt, in Runde 4-6 die Level 2 Gebäudekarten
- Jedes Geäbude außer dem Headquarter und Watchtower kann mit jedem Gebäude überbaut werden (außer den Upgrades für Headquarter und Watchtower)
  - Die Extension kann erst überbaut werden, nachdem sie freigeschaltet wurde
  - Bekämpfen von Plünderern (- AK 47 auf BAM-Symbol) gibt immer einen Punkt, egal ob durch Aktion mit Siedlern, Gebäudeplättchen oder anderes, sofern Plünderer vorhanden sind

## Spielablauf:

### Vorbereitung:
- Spielermarker auf Plündererleiste entsprechend Runde (Nicht verdecktes Feld rechts vom Rundenmarker) vorrücken
- Rundenmarker 1 vorsetzen
- Schadensleiste auswerten, es wird *jedes Feld links* vom Schadensmarker ausgewertet, liegt der Schadensmarker auf dem 1. Feld, wird er umgedreht (+2 Punkte)
- Ausrüstungskarten (so viele wie Spieler) aufdecken und von links nach rechts auslegen
  - Überlebende aus dem Beutel ziehen (Zahl steht über Auslagefeld) und auf Ausrüstungskarten legen (1. Runde max. 1 Plünderer)
- Gebäudekarten (so viele wie Spieler - Level beachten) aufdecken und von links nach rechts auslegen
  - 1 Gebäudeplättchen offen auf jede Gebäudekarte legen

### Bieten:
- Startspieler bestimmen (der mit den meisten Punkten, bei Gleichstand der oben liegende Spielermarker, also der letzte, der auf das Feld gekommen ist)
- Ab Startspieler immer im Uhrzeigersinn jeweils einen Spielermarker + beliebig viele Worker (auch 0 möglich) in die oberste, freihe Reihe eines beliebigen Bietfeldes setzen

- Bonus und Malus erhalten jeweils der Spieler mit bestem bzw. schlechtesten Gebot. Bei Gleichstand zählt wer weiter oben ist
  - Bonus und Malus werden jetzt sofort alle ausgeführt

### Auswertung:
- Immer in Reihenfolge der Gebote (vor der Auswertung werden alle Boni/Mali ausgewertet!)

#### Erkunden:
- Ausrüstungsgegenstand aussuchen, Siedler nehmen und hinter Sichtschirm, Plünderer kommen direkt wieder in den Beutel und Plündererleiste wird entsprechend erhöht

#### Bauen:
- Gebäude aussuchen, Gebäude in Siedlung bauen oder in Schachtel zurücklegen
- Bauen ist auf jedem Platz möglich, egal ob frei oder besetzt außer:
  - Headquarters / Watchtower beachten (hier nur Upgrades möglich und Upgrades nur hier oder auf Tower Site möglich)
  - Extension überbauen erst wenn freigeschaltet
- Gebäudeplättchen (Bonus/Malus der auf dem Gebäude liegt) ausführen

#### Stadt:
- Beginnen mit dem Startspieler die im Bietbereich eingesetzten Siedler verwenden. Siedler auf Aktionen stellen um zu tracken

#### Abschluss:
- Alle Siedler hinter Sichtschirm
- Für jeden Plünderer auf der Plündererleiste Schadensmarker 1 nacht rechts, wenn Schadensmarker ganz rechts -1 Punkt für jeden weiteren Schaden
- Spielermarker auf Plündererleiste bleibt stehen!
- Unterkunftspunkte auswerten, müssen gleich oder größer Zahl der Siedler sein. Für jeden Siedler zu viel muss ein Siedler zurück in den Beutel und es gibt -2 Punkte.
- Gebäude mit festen Punktzahlen geben diese Punkte jetzt (ja, jede Runde)

##### Zwischenwertung nach 3. Runde
- 1. öffentliches Wertungsplättchen wird für jeden gewertet und aus dem Spiel genommen
- 3 Bietplättchen durch 3 neue gezogene austauschen
- Jeder Spieler verliert Punkte entsprechend der Zahl seiner Plünderer
- Spielermarker auf Plündererleiste zurück auf das erste Feld (vor der 1)


## Spielende
- Nach der 6. Runde endet das Spiel
- Jeder Spieler wertet das 2. öffentliche Wertungsplättchen
- Jeder Spieler sucht sich **1** privates Wertungsplättchen aus und wertet es
- Die Schadensleiste von jedem Spieler wird nochmal gewertet
- Für jeden Plünderer verliert jeder Spieler einen Punkt
- Bei Punktegleichstand gewinnt der Spieler mit mehr Ausrüstungskarten