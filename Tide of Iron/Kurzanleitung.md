# Tide of Iron - Kurzanleitung

## Aufbau
1. In Initiative Reihenfolge Starteinheiten in die Deployment Zone platzieren
2. In Initiative Reihenfolge Starteinheiten nach Belieben in Op Fire setzen

## Rundenablauf

### Action Phase

In Initiative Reihenfolge machen beide Spiele abwechselnd die vom Scenario vorgegebene Anzahl (Im 4-Spieler Spiel Anzahl/2 aufgerundet pro Spieler, immer beide Spieler eines Teams direkt hintereinander) an Actions (ihren Action Turn)

#### Mögliche Actions
Nach jeder der folgenden Actions (außer Prepare Op Fire) sind die beteiligten Einheit fatigued!
##### Advance (Movement only)
Fresh Unit bis zu volles Movement bewegen

##### Concentrated Fire (Fire only)
Fresh Unit greift mit voller Angriffskraft an

##### Prepare Op Fire
Eine fresh Unit in Op Fire setzen

##### Fire and Movement
Einen Squad Unit bis zu **Movement -1** bewegen
**oder**
ein Vehicle bis zu **Movement -2** bewegen
**danach**
mit halber Angriffskraft angreifen

- keine Long-range Attacks möglich
- Fire and Movement kann nicht durch verbündete Einheiten mit Combined Fire unterstützt werden
- Squads mit Heavy Infatry Weapon können keine Fire and Movement Action machen
- Man darf den Angriff nach dem Movement auch ausfallen lassen

##### Activate Strategy Card
Strategy Karte mit Keyword **Action phase:** auswählen, Command bezahlen und Effekt ausführen

##### Assault
Einen fresh Squad bewegen (Movement -1) und dann ein adjacent hex (darf keine gegn. heavy Vehicles enthalten) angreifen (normal Attack, niemals suppressive), dass min. eine gegnerische Unit enthält:

Abwicklung:

1. Der active Player wählt bis zu zwei Squads (**auch fatigued, aber nicht pinned oder disrupted**) in hexes adjacent zum Target Hex aus, die den Assault Attack unterstützen (halbe firepower)
2. Angreifer Angriffskraft ermitteln (Wert gegen Infantry, auch wenn im Target Hex ein light Vehicle ist)
3. Verteidiger Angriffskraft aller Squads und light Vehicles (**liefern keinen Armor Bonus**) im Target Hex ermitteln
    - pinned Squads liefern halbe Angriffskraft (es sei denn es ist ein Officer im Hex vorhanden)
    - disrupted Squads können nicht unterstützen
4. Angreifer würfelt schwarze Würfel entsprechend seiner Angriffskraft, Verteidiger rote Würfel entsprechend seiner Angriffskraft + Cover (Bonus von fortifications ist komulativ)
    - Trefferanzahl des Angreifers: Successes der schwarzen Würfel (4,5,6) minus roten Würfel (5,6)
5. Verteidiger würfelt schwarze Würfel entsprechend seiner Angriffskraft, Success (4,5,6)
    - Dies entspricht der Trefferanzahl des Verteidiger, der Angreifer kann nicht verteidigen!
6. Der Angreifer entfernt so viele Figuren, wie der Verteidiger Schaden verursacht hat. Falls dadurch die active Unit vernichtet wird, muss weiterer Schaden auf die participating Units verteilt werden
7. Der Verteidiger entfernt so viele Figuren, wie der Angreifer Schaden verursacht hat
    -  Hits dürfen auch auf defending light Vehicles verteilt werden, aber nicht mehr, als nötig sind, um das light Vehicle zu zerstören
8. Wenn die Trefferanzahl des Verteidigers >= der Trefferanzahl des Angreifers ist, ist der Assault Attack erfolglos, alle angreifenden Einheiten werden fatigued
9. Wenn die Trefferanzahl des Angreifers > Trefferanzahl des Verteidigers ist, ist der Assault Attack erfolgreich, der Verteidiger muss alle Units aus dem Target Hex retreaten
    - Der Retreat muss in **ein** adjacent Hex erfolgen
    - Retreat ist nicht möglich in Felder, die gegnerische Einheiten enthalten oder wenn das Stacking Limit überschritten würde
    - Retreating Units die nicht in dieses Hex retreaten können, werden vernichtet
    - Disrupted Units können nicht retreaten, sondern werden sofort zerstört
    - Pinned Units, die retreaten sind danach automatisch disrupted
    - Retreating aktiviert kein Op Fire
    - Units, die retreaten, sind danach grundsätzlich fatigued
10. Nachdem der Verteidiger das Feld geräumt hat, **dürfen** alle angreifenden Units (die vor dem Angriff nicht fatigued waren) in das Target Hex vorrücken
    - Das Vorrücken ist kein normales Movement und aktiviert kein Op Fire

- Squads mit Heavy Infatry Weapon können keine Assault Action machen
- Man darf den Angriff nach dem Movement auch ausfallen lassen

##### Special Actions
z.B. Aktionen von Strategy Cards nutzen, Tokens wie das Medic Token einsetzen

### Command Phase
1. Determine Control over Objectives
  - Steht eine Unit auf einem Objective, werden evtl. vorhandene gegnerische Control Marker entfernt und eigene platziert
  - bei Hexes ohne Units ändert sich nichts am Control, Controll Marker verbleiben im Hex
2. Receive Command and Victory Points
3. Spend Command
    - In Initiative Reihenfolge so viel Command ausgeben, wie man möchte:
        - Activate Strategy Cards (Keyword **Command Phase:**)
	    - Increase Initiative Pool (Initiative verbleibt im Pool, es sei denn es wird durch Karten entfernt!)
4. Determine Initiative
  - bei Gleichstand im Initiative Pool, wechselt die Initiative!

### Status Phase
1. Draw Strategy Card(s)
    - ist auf der ersten gezogenen Karte ein rotes Plus, darf **eine** weitere gezogen werden
2. Remove Tokens
    - alle Activation Tokens entfernen
    - alle Condition Tokens runterstufen (Disrupted -> Pinned, Pinned -> entfernen, **Officer im Hex vorhanden: Disrupted -> entfernen**)
    - expiring Markers entfernen (z.B. Rauch)
3. Place Units in Op Fire Mode
  - In Initiative Reihenfolge
4. Squad Tranfers
  - In Initiative Reihenfolge dürfen innerhalb eines Hexes Figures zwischen Squads getauscht werden, mit folgenden Einschränkungen:
    - Aus oder in Squads mit Specialization Tokens dürfen keine Figures verschoben werden
	- Specialization Tokens dürfen niemals verschoben werden
	- Aus oder in Squads, die pinned oder disrupted sind
	- Aus oder in Squads in Entrenchments oder Pillboxes
5. ggf. Reinforcements und Events (abhängig vom Scenario)
6. Rundenmarker weitersetzen

## FAQ
### Specialization Tokens 
- Niemals Specialization Tokens in Squads mit Heavy Infantry Weapon

### Stacking Limits
- Max 3 Units in einem Hex
- Max 2 Fahrzeuge in einem Hex

