## Anleitungen/Infos:
- [Bitbucket Markdown Anleitung](https://bitbucket.org/tutorials/markdowndemo/overview)

- [Markdown in PDF konvertieren](http://www.markdowntopdf.com/)

- AsciiDoc in PDF konvertieren mit asciidoctor-pdf gem (mit choco ruby installieren, mit gem dann asciidoctor und asciidoctor-pdf)
  - PowerShell:  asciidoctor-pdf .\Kurzanleitung.adoc

## Bitbucket Tastaturkürzel:
![Tastaturkürzel](http://blog.bitbucket.org/files/2013/02/keyboard-shortcuts.jpeg)