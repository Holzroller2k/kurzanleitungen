# Kurzanleitung:

## Begriffserläuterungen:
- Gefahr: Monstermarker oder Unglücksmarker in einer Region
  - Monstermarker können beliebig viele in einer Region liegen, der kämpfende Held darf sich das Monster aussuchen
  - Unglücksmarker kann nur ein einziger liegen, soll ein weiterer dazu passiert nichts
  


## Vorbereitung:
- Alle Karten nach Rückseite sortieren, mischen
- Auf jede der 3 oberen Gefahrenzonen einen bronze Monstermarker
- Auf jede der 3 unteren Gefahrenzonen einen Unglücksmarker
- Jeder Held zieht zwei Questkarte vom auf dem Heldenbogen angegebenen Stapel, eine behalten, eine ablegen
- Jeder Held legt seinen zweiten Heldenmarker auf die Fahne der Unterstützung, die dem Typ seiner aktuellen Unterstützungsquest entspricht

## Aktionen:
- Reisen
  - 1 oder 2 Straßen weit reisen, Hinweismarker nur vom Zielort

## Besondere Regeln/Hinweise/Limits:




## Spielablauf:


## Spielende
- Ein Spieler schließ seine dritte Hauptquest ab, alle anderen Spieler haben noch einen Zug