# Kurzanleitung:
## Einleitung:
### Die Hofkarten:
- Links Feld, Oben Marktkarren, Unten Helfer, Rechts Hofausbau.
- Links und Rechts unbegrenzt anlegen (Rechts Kosten bezahlen; 1,2,3...)
- Oben und Unten max. 3 Karten. Wenn 3 liegen darf eine abgelegt und ausgetauscht werden

### Begriffserläuterungen:
- Erntegut: Oliven, Korn, Trauben
- Ressource: Erntegut, Schweine
- Aufgewertetes Gut: Nahrung, Wein, gepökeltes Fleisch
- Hofgut: Erntegut, Schwein, Aufgewertetes Gut, Silber, Siegpunkt

## Vorbereitung:
- 1 zufällig gezogener Dachmarker pro Runde und Spieler
- (2 pro Spieler + 1) Ertragswürfel
- 1 Siegpunkt vor jedes Handwerksgebäude
- Jeder Spieler gleichen Satz aus 4 Eselmarker, 1 Silber, 1 Siegpunkt, 1 Handelsware, 4 Hofkarten
- Startspieler auf der Siestaleiste nach oben, Rest in Reihefolge darunter
- Startreihenfolge ab Startspieler im Uhrzeigersinn

## Besondere Regeln/Hinweise/Limits:
- Erntegüter werden niemals zwischen Feld und Lager verschoben
- Marktstände mit einem X nicht mit 2-3 Spielern
- Dachmarker können *einmal* im Spiel eingesetzt werden, dann umdrehen
- Siegpunkte dürfen geheim gehalten werden
- Jeder Spieler kann jeden Handwerksmarker von jedem der Handwerksgebäude 1x bekommen
- Spielermarker sind limiert
- Pro Hof: Oben 3 Marktkarren, Unten 3 Helfer, Links und Rechts beliebig viele Felder oder Hofausbauten
- Das Standard-Handkartenlimit ist 3

## Spielablauf:
### Jederzeit Aktionen:
Unabhängig von den vier Phasen darf man, wenn man am Zug ist:
- Handelswaren und Dachmarker einsetzen
- Ressourcen kaufen und verkaufen
- Ressourcen aufwerten

### Hofphase (Blau)
#### Teil 1
- Jeder *darf eine* Hofkarte auspielen (**In erster Runde muss jeder 2 auspielen**))
  - Jeder Hofausbau (rechts anlegen) kosten die oben rechts auf dem Hoftableau angegebenen Kosten
- Hofkarten auf jetzt gültiges Handkartenlimit nachziehen/abwerfen

#### Teil 2
- Jeder nimmt sein Einkommen

#### Teil 3
- Auf allen freien Feldern wächst ein Erntegut
- Wenn min. 2 Schweine und Platz vorhanden, 1 Schwein dazu

#### Teil 4
- Dachmarker kaufen:
  - Runde 1: Umgekehrte Spielerreihenfolge
  - Runde 2+: Spielerreihenfolge  
  
### Ertagsphase (Grün)
- Startspieler würfelt mit allen Ertragswürfeln und verteilt sie auf Ertragsfelder
- In Spielerreihenfolge nehmen sich alle 1 Würfel, führen die Ertragsaktion aus bis 1 Würfel verbleibt, diese Aktion führen alle aus

### Transportphase (Grau)
#### Besonderheiten:
- Einmal gewählte Eselmarker stehen nicht mehr zur Verfügung, Reset für Runde 4
- Jede Lieferung bewegt *ein* Hofgut zu einem Marktkarren oder zu einem Handwerksgebäude
- Abgeschlossene Karren werden weggeschickt, Siegpunkte und Handelsware einkassiert
  - Dann sucht der Spieler einen freien (wenn Möglich, sonst besetzten entfernen) Markstand mit der Zahl des Karrens, alle angrenzenden niedrigeren Marker anderer Spieler werden entfernt, **1 Siegpunkt pro entferntem Marker**
- Wurde eine Reihe in einem Handwerksgebäude abgeschlossen bekommt derjenige das Handwerkerplättchen, führt die Sofortaktion aus, erhält Rundenzahl als Siegpunkte, platziert Spielermarker auf Fläche vor Gebäude und nimmt ggf. den Siegpunkt

- Mit jedem Handwerksgebäude in dem zum ersten mal eine Reihe abgeschlossen wurde, wird in Reihenfolge eines der 3 gesperrten entsperrt. Der auslösende Spieler erhält den Siegpunkt

-----------------------

1. Jeder wählt verdeckt einen Eselmarker aus
2. Vorrücken auf Siestaleiste
	- **Die neue Spielerreihenfolge gilt sofort, Siestaleiste bleibt noch unverändert!**
3. Lieferungen ausführen
4. Zusatzlieferungen kaufen und ausführen, alle auf einmal kaufen, alle sofort ausführen
  
### Wertungsphase (Rot)
- Jeder 1 Siegpunkt pro Marktstand
- Siegpunkte laut Siestaleiste
- Siestaleiste zurücksetzen, Startspieler nach oben, rest sortiert drunter
- Neue Dachmarker aufdecken, alte aus dem Spiel entfernen

## Spielende
- Das Spiel endet nach 6 Runden
- Jeder Spieler macht noch:
  - Ressourcen und Handelswaren werden in Geld getauscht
  - Geld wird 5:1 in Siegpunkte getauscht
  
Es gewinnt der Spieler mit den meisten Siegpunkten, bei Gleichstand Restsilber, sonst mehrere Gewinner

**Idee:** Bei Gleichstand Zahl der Hofausbauten

