# Kurzanleitung - Terraforming Mars:
## Einleitung:
- Die Spieler versuchen im Jahre 2315 mit ihren Mega-Konzernen den Mars für die Besiedlung vorzubereiten. Es gewinnt der Spieler mit den meisten Siegpunkten (VP)

### Begriffserläuterungen:
- Global Parameters: Temperature, Oxygen, Ocean
- Terraform Rating: Bestimmt das Basis-Einkommen und die Basis-Punkte

![Karten-Legende](Karten-Legende.jpg)

## Vorbereitung:
- 9 Ocean Tiles auf ihr Feld, Temperature und Oxygen Marker auf Startposition, Generation Marker auf 1.
- Sicherstellen, dass keine "Corporate Era" (weißes Dreieck auf rotem Grund in unterer, linker Ecke  - Werden nur im *Extended Game verwendet*) cards im Project Deck oder zwischen den Corporation Cards sind. Project Cards dann mischen.
- Gewinner der letzten Partei wird Startspieler, Player Markers auf Feld 1 auf jedem Track auf dem Playerboard und 1 Player Marker auf 20 auf dem TR Track.
- Neue Spieler erhalten eine Beginner Corporation Card, andere Spieler 2 Normal Corporations und 10 Project Cards. 
-- Beginner Corporation dürfen alle 10 Project Cards gratis behalten. 
-- Normal Corporations zahlen pro Project Card die sie behalten wollen 3M€.
- In Spielerreihenfolge decken die Spieler ihre Corporation auf, erhalten die Startressourcen und Production die auf der Corporation Card stehen und zahlen für die Project Cards, die sie behalten.
-- Abgelegte Projects Cards werden *face down* auf den Ablagestapel gelegt
- Dann startet die erste Generation, ohne eine Player Order Phase und ohne eine Research Phase (diese wurde im Setup bereits durchgeführt), der Startspieler beginnt also mit der Action Phase

## Extended Game
Das Extended Game verlängert die Spielzeit und -komplexität

- Alle Corporate Era cardds ins Spiel nehmen
- Mit Production 0 bei allen Kästen starten

## Allgemeine Regeln / Einsetzregeln
- Jede Erhöhung eines Global Parameters gibt immer einen TR-Punkt für den Spieler, auch das Einsetzen eines Ocean Tiles
- Karten mit einem *Building-Tag* dürfen teilweise oder ganz mit Stahl bezahlt werden - 1 Stahl ist 2 M€ wert
- Karten mit einem *Space-Tag* dürfen teilweise oder ganz mit Titan bezahlt werden - 1 Titan ist 3 M€ wert

- Ein *brauner Rahmen* um ein Symbol bezieht sich auf Produktion
- Ein *roter Rahmen* um ein Symbol heißt, dass man die Ressourcen oder Produktion bei jedem beliebigen Spieler (auch sich selber) entfernen/reduzieren darf
- Ein *roter Rahmen* um ein Tile-Symbol heißt, dass sich es auf ein oder beliebig viele Tiles dieser Art bezieht
- Ein *roter Rahmen*
- Reduzierung von Produktion müssen immer durchgeführt werden, bei *roten Rahmen* im Zweifelsfall auch bei einem selber

- Die Produktion kann niemals unter das aufgedruckte, niedrigste Feld fallen, aber über das höchste steigen. Dann wird einfach ein weiterer Marker verwendet

### Einsetzregeln für Hex-Tiles
- Ocean Hex-Felder und Noctis-City sind für die entsprechenden Felder reserviert
- Symbole in Hex-Felder sind der Einsetzbonus, den der Spieler erhält, der dort ein Tile, egal welches einsetzt

- City Tiles: City-Tiles dürfen nicht neben andere Citys (egal von welchem Spieler) gesetzt werden. Ausnahme: Nur Notic City darf neben existierende City Tiles gesetzt werden
- Greeneries: Greeneries müssen, wenn möglich, angrenzend an ein Tile gesetzt werden, das einem selber gehört
- Ocean Tiles: Dürfen nur auf die reservierten Felder gesetzt werden
  *Wird ein Tile neben einem Ocean-Tile eingesetzt, erhält der einsetzende Spieler 2 M€ pro benachbartem Ocean-Tile*
- Specieal Tiles: Werden durch Karten eingesetzt. Die Einsetzregeln sind auf der entsprechenden Karte vermerkt.

## Besondere Regeln/Hinweise/Limits:
- Es gibt kein Handkartenlimit
- Abgelegte Project Cards liegen immer *face down*
- Tags auf Karten können von anderen Karten referenziert werden z.B. als Requirement
- Karten die einen Global Parameter erhöhen, der schon auf max. Wert ist (bzw. alle Ocean Tiles  bereits gesetzt) dürfen trotzdem gespielt werden

## Spielablauf:

### Player Order Phase

Startspieler geht 1 weiter

### Research Phase
Jeder Spieler Zeit 4 Karten, dann entscheidet jeder Spieler wie viele er davon behält (0-4). Für jede Karte, die er auf seine Hand nimmt, zahlt er 3.

Draft Mechanismus (optional):

- Jeder erhält 4, gibt 3 weiter, gibt 2 weiter...
- Aus den 4 die jeder so erhalten hat, darf jeder dann 0-4 kaufen

### Action Phase

#### Handkarte spielen

1. Requirements prüfen (Effekt muss ausführbar sein!)
2. Kosten zahlen und Immediate Effects ausführen
3. Karte auslegen
  - Events (rotes Banner) werden *face down* auf einen Ablagestabel pro Spieler gelegt
  - Automated Cards (grünes Banner) werden *face up* auf einen versetzen Stapel pro Spieler gelegt, sodass die Tags sichtbar bleiben
  - Active Cards (blaues Banner) werden *face up* auf einen versetzen Stapel pro Spieler gelegt, sodass die Effekte/Aktionen sichtbar bleiben

#### Standardprojekt

Eines der Standardprojekte auf dem Spielplan für die angegebenen Kosten durchführen. Das erste Standardprojekt ist x Karten ablegen und dafür x M€ nehmen.

#### Claim a Milestone

- Wenn die Bedingungen für den Milestone erfüllt sind, 8 M€ bezahlen, Spielermarker auf Claim-Feld (es gibt nur 3, weil nur 3 Milestones geclaimt werden können)
- Jeder Milestone ist am Ende 5 VP wert

#### Fund an Award

- 1. Award Funding kostet 8 M€, 2. 14 M€, 3. 20 M€
- Es ist egal, wer einen Award finanziert hat
- Am Ende gibt es 5 VP für den besten in einer finanzierten Award-Kategorie, 2 VP für den zweitbesten
  - Bei Gleichstand erhalten mehrere Spieler die vollen Punkte
  - Bei mehreren 1st-Platzierten, gibt es keinen 2t-Platzierten

#### Use Action on Blue Card

Jede Action auf blauen Karten kann nur einmal pro Generation verwendet werden. Auf der Karte einen Player Marker platzieren, um zu markieren, dass sie in dieser Generation bereits eingesetzt wurde.

#### Convert Plants into Greenery

8 Plants umwandeln in ein Greenery-Teil das sofort nach den Platzierungsregeln platziert wird

#### Convert Heat into Temperature

8 Heat in ein Global Temperature umwandeln

### Production Phase

- Verbliebene Energie wird 1:1 in Hitze umgewandelt
- TR-Wert erhält  man als M€-Einkommen, zusätzlich zur Produktion auf der Spielermatte
- alles wird in der Menge produziert, wie die Spieler-Marker aktuell liegen

## Spielende
Das Spiel endet *am Ende der Generation*, in der alle 3 Global Parameter auf Max. erhöht werden
- Nach der Production Phase, dürfen alle spieler noch einmal Plants in ein oder mehrere Greenery Tiles umwandeln
- Das Platzieren der Greenery Tiles kann noch Effekte wie Placement Bonuses auslösen

Summiere:
1. TR-Rating
2. Awards
3. Milestones
4. Game Board
5. Cards

Bei Gleichstand gewinnt der Spieler mit mehr Restgeld