# Kurzanleitung:

## Begriffserläuterungen:
- Rohstoff: Holz, Stein, Nahrung
- Ware: Rohstoff, Gold, Arbeiter, Zerstörungs- und Verteidigungsplättchen

## Vorbereitung:
- Reihum jeweils 2 allgemeine Karten, 2 Völkerkarten ziehen

## Besondere Regeln/Hinweise/Limits:
- Es gibt kein Handkartenlimit
- Jede ausliegende Karte ist ein Ort
- Gold ist ein Rohstoffjoker
- Allgemeine Karten werden vom Ablagestapel gemischt und weiterverwendet - Völkerkarten sind endlich
- "Du erhältst jedes mal ..., wenn du ... baust": 
Waren für das Bauen von Orten bestimmmter Farben gibt es auch für den Fähigkeitsort selbst - Orte zählen sich selbst mit
- Wenn man Karten ziehen darf, kann man sich immer zwischen Allgemeinen- und Völkerkarten entscheiden


### Japaner
- **Samurai:** Japaner können beliebig viele Arbeiter, jederzeit während des eigenen Zuges, auf Völkerorte (max 1 pro Ort) stellen, funktioniert wie Verteidigungsplättchen
  - Einmal eingesetzt Samurai können nicht wieder entfernt werden
  - Samurai werden in der Aufräumphase nicht entfernt
  

## Spielablauf:
- 5 Runden
### Kartenphase
- Jeder 1 Völkerkarte
- Spielerzahl + 1 Karten vom Allgemeinen Stapel, vom  Startspieler im Uhrzeigersinn sucht sich jeder eine Karte aus, die übrige Karte wird abgelegt.
- Das gleiche nochmal gegen den Uhrzeigersinn, beginnend mit dem letzten Spieler

###  Ertragsphase
- Völkertafel, Orte und Handelsabkommen produzieren

###  Aktionsphase
- Reihum jeweils 1 Aktion oder passen, bis alle gepasst haben
- Wer gepasst hat kann keine Aktionen mehr machen, und kein Ziel mehr von Aktionen anderer Spieler sein (S. 10 Denkt daran!)

#### Ort bauen
- Jeder Ort kann genutzt werden, wenn ein Ort benötigt wird (es müssen keine Fundamente sein)
- Produktionsorte produzieren beim Bauen sofort, Bauprämien gibt es zusätzlich

#### Handelsabkommen schließen
- 1 Nahrung abgeben
- Handelsabkommen produzieren beim Abschließen sofort

#### Zerstören
- Karte aus der Hand für 1 Zerstörungsplättchen zerstören, Zerstörungsprämie bekommen
- Ort bei Mitspieler zerstören, kostet 2 Zerstörungsplättzchen +1 für jeden Schutz (wie Verteidigungsplättchen, Samurai)
  - Völkerorte können nur bei Japanern zerstört werden - Es können nur Orte mit Zerstörungsfeld zerstört werden
  - Spieler bei denen ein Ort zerstört wurde erhalten 1 Holz
#### Aktionsort aktivieren
- Geforderte Waren auf den Ort legen
- Jeder Aktionsort kann nur 1x verwendet werden, es sei denn es ist anders angegeben
  - Wenn ein Aktionsort mehrmals verwendet werden darf, darf man beide Verwendungen mit einer Aktion durchführen


### Aufräumphase
- Alles  was man nicht lagern kann abwerfen
- Startspieler weitergeben

## Spielende
- Festes Ende nach 5 Runden

**Bei Gleichstand** entscheidet die höhere Summe der übrig gebliebenen Arbeiter und Rohstoffe, herrscht auch hierbei Gleichstand, entscheidet die Anzahl der Handkarten

## Die Atlanter

### Völkerkarten ersetzen
- Barbaren: Urwald durch Verwunschenen Wald
- Japaner: Farm durch Moderne Farm
- Römer: Handelskolonie durch Entfernte Handelskolonie
- Ägypter:  Pyramide durch Mystische Pyramide

### Völkerdeck zusammenstellen
30 Karten, darunter 3 Karten mit 3 Exemplaren, 6 mit je 2, 9 mit je 1. Es müssen alle Exemplare dieser Karte ins Deck.

### Völkerregeln Atlanter
- Verteidigungsplättchen: In der Ertragsphase bekommen die Atlanter so viele wie es Spieler gibt

#### Technologieplättchen
Grundlegende Technologieplättchen:

 - Produktionsorte: Doppelte Produktio
 - Fähigkeitsorte: 2 mal pro Anwendung nutzbar
 - Aktionsorte: Aktion darf 1 mal mehr verwendet werden
 
Fortgeschrittene Wertungstechnologie:

 - Spieler erhält jedes mal einen Siegpunkt, wenn der Ort genutzt wird/produziert
 
Fortgeschrittene Verteidigungstechnologie:

- Wird der oder entfernt (nicht überbaut!) oder zerstört, erhält der Besitzer einen Siegpunkt

- Während der Aktionsphase, aber nur vor oder nach einer Aktion darf der Atlanter beiebig viele Technologieplättchen auf seine Allgemeinen Orte verteilen
- Auf jedem Ort darf sich nur ein Technologieplättchen von jeder Sorte befinden
- Einmal gelegte Technologieplättchen dürfen nicht verschoben werden
- Erhält ein Spiel Fortgeschrittene T-Plättchen, darf er sich aussuchen, ob Wertungs- oder Verteidigungstechnologie
- Technologieplättchen werden in der Aufräumphase nicht abgeräumt
- Grundlegende T-Plättchen kommen zurück in den Vorrat des Spielers, wenn der Ort zerstört, entfernt oder überbaut wird
- Fortgeschrittene T-Plättchen kommen zurück in den allgemeinen Vorrat
- Fortgeschrittene Verteidigungsplättchen geben einen Siegpunkt, wenn der Ort zerstört oder entfernt wird, nicht aber beim Überbauen
- Normale und Fortgeschrittene Verteidigungsplättchen können nicht gleichzeitig auf einem Ort liegen

### Offene Produktionsorte
- 1 Arbeiter zu einem offenen Produktionsort von Mitspieler entsenden, Waren aus allg. Vorrat, Besitzer des Ortes erhält 1 Arbeiter aus allg. Vorrat
- Pro Offenem Produktions nur 2 entsandte Arbeiter pro Runde

### Details zu Karten
Japaner:

- Feng-Shui-Baumeister: Ort hat ein aufgedrucktes Verteidigungsplättchen, es darf kein Verteidigungsplättchen darauf platziert werden
- Feng-Shui-Baumeister: Die Aktion des Ortes zählt nicht als Bauen, d.h. es werden keine Baukosten gezählt und es gibt keine Bauprämien oder sofortige Produktion
- Erfindungen des Herrn Chi: Es kann kein Aktionsort gewählt werden, der keine Aktion mehr übrig hat

Babaren:

- Prachtkerl des Klans: Beide oder müssen entweder Allgemeine oder Völkerorte sein. Einer der Orte muss ein eigener sein
- Traks Flugmaschine: Der gezogene Allgemeine Ort muss sofort zerstört werden
- Technologie-Abwehrtrupp: Der blockierte Ort kann von niemandem genutzt werden, aber entfernt, zerstört und überbaut. Die Blockierung wird nur durch eine Aktion und 1 Zerstörungsplättchen aufgehoben

Ägypter:

- Ohren des Pharao: Der Mitspieler kann den Ort uneingeschränkt weiternutzen, du kannst den Effekt aber nutzen, als läge er bei dir aus

Römer:

- Verteidigungsanlage: Die Aktion zählt nicht als zerstören, lege die Karten direkt als Fundamente aus und erhalte 1 Holz pro Fundament
- Römische Akademie: Gibt max. 1 Siegpunkt


